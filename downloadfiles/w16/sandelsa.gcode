;HEADER
;Start_gcode
G21                     ; Set to metric [change to G20 if you want Imperial]
G90                     ; Force coordinates to be absolute relative to the origin
G28                     ; Home X/Y/Z axis
M117 Printing...
;ENDHEADER
;Layer #
G0 X544.96 Y331.30 Z20.00 F2500
G1 X544.96 Y331.30 Z8.00 F2500
G1 X444.40 Y505.46 Z8.00 F2500
G1 X243.30 Y505.46 Z8.00 F2500
G1 X142.75 Y331.30 Z8.00 F2500
G1 X243.30 Y157.14 Z8.00 F2500
G1 X444.40 Y157.14 Z8.00 F2500
G1 X544.96 Y331.30 Z8.00 F2500
G0 X544.96 Y331.30 Z20.00 F2500
;FOOTER
; end_gcode
G28 X Y
G0 Z100 				; HOME
G90                             ; absolute positioning
;ENDFOOTER
